# NBclassifier

Naive Bayes classifier, really stupid version

## Theory
### Bayes Theory
```
P(category|sentence) = P(category|sentance)*P(category) / P(sentence)

P(!category|sentence) = P(!category|sentance)*P(!category) / P(sentence)
```
If the probability of the sentence being part of the category is higher than not:
```
P(category|sentance)*P(category) > P(!category|sentance)*P(!category)
```
then we can say that it is more likely that the sentence is in that category.

Let's process words rather than sentences:
```
P(category|sentance) = Σ P(word|sentence) 
```
Then:
```
( Σ P(word|category)) * P(category) > ( Σ P(word|!category)) * P(!category)
```
If true, horray!

## To run
```bash
./gradlew wrapper --gradle-version 4.9
./gradlew run --args='sports "a terrible game"'
```
