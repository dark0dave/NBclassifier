package com.personal.classifier.NaiveBayesUtil

import com.personal.classifier.TrainingSet.TrainingSet
import scala.collection.mutable

class DataSetHandler(category:String, trainingSet: TrainingSet) {

  val trainingDataSet: mutable.HashMap[String, Set[String]] = trainingSet.getData

  val totalLines: Int = trainingDataSet.values.flatten.size

  val totalWords: Int = trainingDataSet.values.flatten.flatMap(line=> line.split(" ")).toSet.size

  // P(Category)
  val getPrioriOfCategory: Double = {
    trainingDataSet(category).size.toDouble/totalLines
  }

  // P(!Category)
  val getPrioriOfOtherCategory: Double = {
    1.0 - getPrioriOfCategory
  }

  // Don't Flatten a set
  def getCategoryWords(): Iterable[String] = {
    trainingDataSet.filterKeys(_==category).values.flatten.flatMap(line => line.split(" "))
  }

  def getOtherCategoryWords(): Iterable[String] = {
    trainingDataSet.filterKeys(_!=category).values.flatten.flatMap(line => line.split(" "))
  }
}

