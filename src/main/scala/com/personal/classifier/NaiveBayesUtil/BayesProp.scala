package com.personal.classifier.NaiveBayesUtil

class BayesProp(input:String, dataHandler: DataSetHandler) {

  val inputSentence:Array[String] = input.split(" ")

  // Calculate (Product of P(word|category)) * P(category)
  def categoryGivenInput:Double = findPropability(dataHandler.getCategoryWords(),dataHandler.getPrioriOfCategory)

  // Calculate (Product of P(word|!category)) * P(!category)
  def otherCategoryGivenInput:Double = findPropability(dataHandler.getOtherCategoryWords(),dataHandler.getPrioriOfOtherCategory)

  // Calculate (Product of P(A|B)) * P(B))
  def findPropability(words: Iterable[String],priori:Double):Double = {
    inputSentence
      .map(inputChar => {

        val result = words.filter(_ == inputChar)
        // Laplace smoothing
        (result.size.toDouble + 1) / (words.size + dataHandler.totalWords)

      }).product * priori
  }

}
