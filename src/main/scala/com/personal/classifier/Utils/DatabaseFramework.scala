package com.personal.classifier.Utils

import slick.dbio.{DBIOAction, Effect, NoStream}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

trait DatabaseFramework {
  def getResult(): Vector[(String, String)]
  def pushNewResult(category:String, sentence:String)
  def runSqlCommand[A, B <: NoStream, C <: Effect, R](fn: DBIOAction[A, B, C] => Future[R])(sqlAction: DBIOAction[A, B, C]): R = {
    Await.result(fn(sqlAction), 5 seconds)
  }
}