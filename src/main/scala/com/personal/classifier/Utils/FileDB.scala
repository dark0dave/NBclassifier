package com.personal.classifier.Utils

import slick.jdbc.SQLiteProfile.backend.Database
import slick.jdbc.SQLiteProfile.api._
import scala.concurrent.duration._
import scala.concurrent.Await

object FileDB extends DatabaseFramework {
  private final val categories = TableQuery[Categories]
  private final val db:Database = Database.forConfig("sqlite")
  def run[R] = super.runSqlCommand(db.run[R]) _

  def setup(): this.type = {
    val setup = DBIO.seq(
      categories.schema.create,
      categories ++=  Seq(
        ("sports", "a great game"),
        ("sports", "very clean match"),
        ("sports", "a clean but forgettable game"),
        ("not sports", "the election was over"),
        ("not sports", "it was a close election")
      )
    )
    Await.result(db.run(setup), 5 seconds)
    this
  }

  override def getResult(): Vector[(String, String)] = {
    run[Seq[(String, String)]](categories.result).toVector
  }

  override def pushNewResult(category: String, sentence: String): Unit = {
    run(categories ++= Seq((category, sentence)))
  }
}