package com.personal.classifier.Utils

import slick.jdbc.H2Profile.api._

object InMemDb extends DatabaseFramework {
  private final val stream = getClass.getResourceAsStream("/create.sql")
  private final val db = Database.forConfig("h2mem1")
  def run[R] = super.runSqlCommand(db.run[R]) _

  def setup(): InMemDb.type = {
    val lines = scala.io.Source.fromInputStream(stream).getLines
    db.createSession().prepareStatement(lines.mkString("\n")).execute()
    this
  }

  override def getResult(): Vector[(String, String)] = {
    run[Vector[(String, String)]](sql"select category, sentence from categories".as[(String,String)].transactionally)
  }

  override def pushNewResult(category: String, sentence: String): Unit = {
    run(sqlu"insert into categories values($category,$sentence);".withPinnedSession)
  }
}