package com.personal.classifier.Utils

import slick.jdbc.SQLiteProfile.api._

class Categories(tag: Tag) extends Table[(String, String)](tag, "categories") {
  def category = column[String]("category")
  def sentence = column[String]("sentence")
  def * = (category, sentence)
}
