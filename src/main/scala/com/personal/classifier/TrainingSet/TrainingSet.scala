package com.personal.classifier.TrainingSet

import scala.collection.mutable
import com.personal.classifier.Utils.DatabaseFramework

class TrainingSet(dataBase: DatabaseFramework) {

  var resultSet: mutable.HashMap[String,Set[String]] = mutable.HashMap[String,Set[String]]()
  def getData: mutable.HashMap[String, Set[String]] = dataBase.getResult().foldLeft(resultSet)((a, b)=> {
    if(a.contains(b._1)) {
      a(b._1) = a(b._1).union(List(b._2).toSet)
    } else {
      a(b._1) = List(b._2).toSet
    }
      a
  })

  def getCategories = List("sports", "not sports")
}
