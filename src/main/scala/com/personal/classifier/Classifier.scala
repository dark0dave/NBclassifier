package com.personal.classifier

import com.personal.classifier.TrainingSet.TrainingSet
import com.personal.classifier.Utils.FileDB
import NaiveBayesUtil._

object Classifier {
  def main(args: Array[String]): Unit = {

    if (args.length != 2) {
      println("Expects: <Category> <Input String>")
      sys.exit(1)
    }

    val Array(category, input) = args

    val trainingSet = new TrainingSet(FileDB.setup())


    val dataHandler = new DataSetHandler(category, trainingSet)

    val bayesProp = new BayesProp(input, dataHandler)

    val categoryProp = bayesProp.categoryGivenInput
    val notCategoryProp = bayesProp.otherCategoryGivenInput

    if(categoryProp > notCategoryProp) {
      println(s"Matches Category, $categoryProp > $notCategoryProp")
      FileDB.pushNewResult(category,input)
    }
    else {
      println("Does not match")
    }
  }
}
